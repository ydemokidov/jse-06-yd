package com.t1.yd.tm;

import com.t1.yd.tm.constant.ArgumentConstant;
import com.t1.yd.tm.constant.CommandConstant;

import java.util.Scanner;

public class TaskManager {

    public static void main(String[] args) {
        showWelcome();
        processArguments(args);

        Scanner scanner = new Scanner(System.in);

        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("ENTER COMMAND:");
            processCommand(scanner.nextLine());
        }
    }

    private static void processArguments(String[] args) {
        if (args == null || args.length == 0) return;
        processArgument(args[0]);
    }

    private static void processArgument(String arg) {
        switch (arg) {
            case ArgumentConstant.ABOUT:
                showAbout();
                break;
            case ArgumentConstant.VERSION:
                showVersion();
                break;
            case ArgumentConstant.HELP:
                showHelp();
                break;
            default:
                showArgumentError();
        }
    }

    private static void processCommand(String arg) {
        switch (arg) {
            case CommandConstant.ABOUT:
                showAbout();
                break;
            case CommandConstant.VERSION:
                showVersion();
                break;
            case CommandConstant.HELP:
                showHelp();
                break;
            case CommandConstant.EXIT:
                exit();
                break;
            default:
                showCommandError();
        }
    }

    private static void exit() {
        System.exit(0);
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Author: Yuriy Demokidov");
        System.out.println("Email: ydemokidov@t1-consulting.ru");
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s, %s : Show info about program \n", CommandConstant.ABOUT, ArgumentConstant.ABOUT);
        System.out.printf("%s, %s : Show program version \n", CommandConstant.VERSION, ArgumentConstant.VERSION);
        System.out.printf("%s, %s : Show arguments list \n", CommandConstant.HELP, ArgumentConstant.HELP);
    }

    private static void showWelcome() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
    }

    private static void showArgumentError() {
        System.err.println("[ERROR]");
        System.err.println("Argument is not supported");
        System.exit(1);
    }

    private static void showCommandError() {
        System.err.println("[ERROR]");
        System.err.println("Command is not supported");
    }

}
